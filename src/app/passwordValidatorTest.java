package app;

import static org.junit.Assert.*;

import java.util.Arrays;

import org.junit.Test;

public class passwordValidatorTest {

	public static int count = 0;
	public String passwordStr;
	public void tearDown() throws Exception{
		
	}
	@Test
	public void testCheckPasswordLengthString() {
		boolean result = passwordValidator.checkPasswordLength("lskjoiw234");
		System.out.println(result);
		assertTrue("Invalid length" , result);
	}
	
	@Test
	public void testCheckPasswordLengthStringException() {
		boolean result = passwordValidator.checkPasswordLength("lskjoiw");
		System.out.println(result);
		assertTrue("Invalid length", result);
	}
	
	@Test
	public void testCheckPasswordLengthStringBoundaryOut() {
		boolean result = passwordValidator.checkPasswordLength("lskjoiw234");
		System.out.println(result);
		assertTrue("Invalid length", result);
	}
	
	@Test
	public void testCheckPasswordLengthStringBoundaryIn() {
		boolean result = passwordValidator.checkPasswordLength("lskjoiw23");
		System.out.println(result);
		assertTrue("Invalid length", result);
	}

	@Test
	public void testCheckDigitsString() {
		boolean result = passwordValidator.checkDigits("lskjoiw2311");
		System.out.println(result);
		assertTrue("Invalid length", result);
	}
	
	@Test
	public void testCheckDigitsStringException() {
		boolean result = passwordValidator.checkDigits("lskjoiw3");
		System.out.println(result);
		assertTrue("Invalid length", result);
	}
	
	@Test
	public void testCheckDigitsStringBoundaryOut() {
		boolean result = passwordValidator.checkDigits("lskjoiw232");
		System.out.println(result);
		assertTrue("Invalid length", result);
	}
	
	@Test
	public void testCheckDigitsStringBoundaryIn() {
		boolean result = passwordValidator.checkDigits("lskjoiw23");
		System.out.println(result);
		assertTrue("Invalid length", result);
	}

}
